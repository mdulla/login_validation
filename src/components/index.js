import { useState, useEffect } from "react";
import "./style.css";
// Validation of the Loginpage.
function Login() {
  const initialvalues = { username: "", password: "" };
  const [formvalues, setFormvalues] = useState(initialvalues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmit, setIsSubmit] = useState(false);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormvalues({ ...formvalues, [name]: value });
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    setFormErrors(validate(formvalues));
    setIsSubmit(true);
  };
  useEffect(() => {
    console.log(formErrors);
    if (Object.keys(formErrors).length === 0 && isSubmit) {
      console.log(formvalues);
    }
  }, [formErrors]);

  const validate = (values) => {
    const errors = {};
    // const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
    const regex = /^.*(?=.{5,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&]).*$/;

    if (!values.username) {
      errors.username = "username is required !";
    } else if (values.username.length < 6) {
      errors.username = "username must be more than 6 characters";
    }
    if (!values.password) {
      errors.password = "password is required !";
    } else if (!regex.test(values.password)) {
      errors.password =
        "Password must contain atleast one special character, one Upper case, a lower case and a number";
    }

    return errors;
  };

  return (
    <>
      <div className="container">
        {Object.keys(formErrors).length === 0 && isSubmit ? (
          <div className="ui message success">signed in successfully</div>
        ) : (
          <pre>{JSON.stringify(formvalues, undefined, 2)}</pre>
        )}
        <form onSubmit={handleSubmit}>
          <div className="login-form" action="">
            <h1>Login</h1>
            <div className="form-input">
              <input
                type="text"
                name="username"
                id="username"
                placeholder=" name"
                autocomplete="off"
                className="form-control"
                required
                value={formvalues.username}
                onChange={handleChange}
              />
              <label for="username">Username</label>
            </div>
            <p>{formErrors.username}</p>

            <div className="form-input">
              <input
                type="password"
                name="password"
                id="password"
                placeholder="password "
                autocomplete="off"
                className="form-control"
                required
                value={formvalues.password}
                onChange={handleChange}
              />

              <label for="password">Password</label>
            </div>
            <p>{formErrors.password}</p>
            <button type="submit" className="btn">
              Login
            </button>
          </div>
        </form>
      </div>
    </>
  );
}
export default Login;
